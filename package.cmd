@echo off
cls

:CLEAN
rmdir /S /Q build
rmdir /S /Q dist
rmdir /S /Q monkey.google.api.egg-info

if defined VIRTUAL_ENV goto RUN

SETLOCAL
set LOCAL_VIRTUAL_ENV=1
call .\.venv\Scripts\activate.bat

:RUN

python setup.py sdist bdist_wheel

if not defined LOCAL_VIRTUAL_ENV GOTO END

call .\.venv\Scripts\deactivate.bat

endlocal

:END
