Monkey Google API
=================

Helper classes for `Google API`_.

Installation guide
------------------

You can install using `pip`_::

    $ pip install monkey-google-api

User guide
----------

.. _Google API: https://developers.google.com/gsuite/products
.. _pip: https://pip.pypa.io/en/stable/